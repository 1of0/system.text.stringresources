﻿using System.Linq;
using System.Threading;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace System.Text.StringResources
{
	/// <summary>
	/// Helper class for managing unmanaged library references.
	/// </summary>
	internal class UnmanagedLibrary : IDisposable
	{
		#region ... Unmanaged imports ...

		[DllImport("kernel32.dll", EntryPoint = "LoadLibrary")]
		private static extern IntPtr LoadUnmanagedLibrary(string name);


		[DllImport("kernel32.dll", EntryPoint = "FreeLibrary")]
		private static extern bool FreeUnmanagedLibrary(IntPtr handle);

		#endregion

		#region ... Static implementation ...

		/// <summary>
		/// Represents a time span during which an <see cref="UnmanagedLibrary"/> instance must be
		/// unreferenced to be marked unreferenced.
		/// </summary>
		private static readonly TimeSpan UnreferencedTimeout = TimeSpan.FromSeconds(10);

		/// <summary>
		/// Holds a synchronization object.
		/// </summary>
		private static readonly object SyncRoot;

		/// <summary>
		/// Holds a collection of unmanaged libraries that are currently loaded into memory.
		/// </summary>
		private static readonly List<UnmanagedLibrary> LoadedLibraries;

		/// <summary>
		/// Static constructor that starts the Janitor thread
		/// </summary>
		static UnmanagedLibrary()
		{
			SyncRoot = new object();
			LoadedLibraries = new List<UnmanagedLibrary>();

			new Thread(Janintor) { IsBackground = true }.Start();
		}

		/// <summary>
		/// Acquires the handle for the unmanaged library with the provided <paramref name="name"/>, and increases the
		/// reference count for that library. If the library is not loaded yet, it will be loaded into memory. For the 
		/// library to automatically unloaded from memory, the acquired handle must be released by calling
		/// <seealso cref="ReleaseHandle(string)"/> or <seealso cref="ReleaseHandle(IntPtr)"/>.
		/// </summary>
		/// <param name="name">Name of the unmanaged library</param>
		/// <returns>Handle that points to the location of the unmanaged library</returns>
		/// <exception cref="DllNotFoundException">Thrown if an error occurs while loading the library</exception>
		public static IntPtr AcquireHandle(string name)
		{
			lock (SyncRoot)
			{
				UnmanagedLibrary instance = LoadedLibraries.FirstOrDefault(library => library.Name == name);
				if (instance == null)
				{
					instance = new UnmanagedLibrary(name);
					LoadedLibraries.Add(instance);
				}
				else
				{
					instance.Reference();
				}
				return instance.Handle;
			}
		}

		/// <summary>
		/// Schedules the unmanaged library with the provided <paramref name="name"/> to be released.
		/// </summary>
		/// <param name="name">Name of the unmanaged library</param>
		public static void ReleaseHandle(string name)
		{
			lock (SyncRoot)
			{
				UnmanagedLibrary instance = LoadedLibraries.FirstOrDefault(library => library.Name == name);
				if (instance != null)
				{
					instance.Dereference();
				}
			}
		}

		/// <summary>
		/// Schedules the unmanaged library with the provided <paramref name="handle"/> to be released.
		/// </summary>
		/// <param name="handle">Handle of the unmanaged library</param>
		public static void ReleaseHandle(IntPtr handle)
		{
			lock (SyncRoot)
			{
				UnmanagedLibrary instance = LoadedLibraries.FirstOrDefault(library => library.Handle == handle);
				if (instance != null)
				{
					instance.Dereference();
				}
			}
		}

		// ReSharper disable once FunctionNeverReturns
		/// <summary>
		/// Janitor thread that periodically frees all unreferenced libraries.
		/// </summary>
		private static void Janintor()
		{
			while (true)
			{
				lock (SyncRoot)
				{
					var unreferencedLibraries = LoadedLibraries.Where(library => library.IsUnreferenced).ToList();

					foreach (UnmanagedLibrary library in unreferencedLibraries)
					{
						library.Dispose();
						LoadedLibraries.Remove(library);
					}
				}
				Thread.Sleep(10000);
			}

		}

		#endregion

		#region ... Non-static implementation ...

		/// <summary>
		/// Holds the number of references to the unmanaged library.
		/// </summary>
		private int _referenceCount;

		/// <summary>
		/// Holds either a <see cref="DateTime"/> instance that marks the time at which the 
		/// <see cref="UnmanagedLibrary"/> instance can be freed, or null if there are still references to the library.
		/// </summary>
		private DateTime? _markUnreferencedAt;

		/// <summary>
		/// Gets the name of the unmanaged library
		/// </summary>
		public string Name { get; private set; }

		/// <summary>
		/// Gets the handle that points to the location of the unmanaged library.
		/// </summary>
		public IntPtr Handle { get; private set; }

		/// <summary>
		/// Gets a boolean value indicating whether or not the unmanaged library has any references, and if not, whether
		/// it has been unreferenced over the time span of <see cref="UnreferencedTimeout"/>.
		/// </summary>
		public bool IsUnreferenced
		{
			get
			{
				return _markUnreferencedAt != null
					&& _markUnreferencedAt < DateTime.Now;
			}
		}

		/// <summary>
		/// Initializes an instance of the <see cref="UnmanagedLibrary"/> class, representing a unmanaged library of the
		/// provided <paramref name="name"/>. During the initialization, the library of the provided name will be loaded
		/// into the program memory, and a pointer of its address will be stored in the <see cref="Handle"/> parameter.
		/// After initialization the reference count will be set to 1.
		/// </summary>
		/// <param name="name">Name of the unmanaged library to load</param>
		/// <exception cref="DllNotFoundException">Thrown if LoadLibrary returns a null-pointer</exception>
		public UnmanagedLibrary(string name)
		{
			IntPtr handle = LoadUnmanagedLibrary(name);

			if (handle == IntPtr.Zero)
			{
				throw new DllNotFoundException(String.Format("Failed loading native library {0}", name));
			}

			Name = name;
			Handle = handle;
			Reference();
		}

		/// <summary>
		/// Increases the reference count to the unmanaged library by one.
		/// </summary>
		public void Reference()
		{
			lock (SyncRoot)
			{
				_referenceCount++;
				_markUnreferencedAt = null;
			}
		}

		/// <summary>
		/// Decreases the reference count to the unmanaged library with one, and if zero, sets a time stamp at which the
		/// instance may be marked unreferenced.
		/// </summary>
		public void Dereference()
		{
			lock (SyncRoot)
			{
				_referenceCount--;

				if (_referenceCount == 0)
				{
					_markUnreferencedAt = DateTime.Now + UnreferencedTimeout;
				}
			}
		}

		/// <summary>
		/// Clears the unmanaged library from memory.
		/// </summary>
		public void Dispose()
		{
			lock (SyncRoot)
			{
				FreeUnmanagedLibrary(this.Handle);
			}
		}

		#endregion
	}
}
