﻿
namespace System.Text.StringResources
{
	/// <summary>
	/// Common ancestor for the <see cref="StringResource"/> and <see cref="StringResourceFactory"/> classes. In
	/// addition, an implementor should also implement the implicit operator for the string type.
	/// </summary>
	public interface IStringResource
	{
		/// <summary>
		/// Should apply the provided <paramref name="transformation"/> on the current instance.
		/// </summary>
		/// <param name="transformation">Text transformation to apply</param>
		/// <returns>Chainable self</returns>
		StringResource Transform(TextTransformation transformation);

		/// <summary>
		/// Should strip the current instance from mnemonic indicators.
		/// </summary>
		/// <returns>Chainable self</returns>
		StringResource StripMnemonics();

		/// <summary>
		/// Should interpolate the parameters in the current instance using the provided <paramref name="arguments"/>.
		/// </summary>
		/// <param name="arguments">Arguments to fill the placeholders with</param>
		/// <returns>Chainable self</returns>
		StringResource Interpolate(params string[] arguments);
	}
}
