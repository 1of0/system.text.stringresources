﻿
namespace System.Text.StringResources
{
	/// <summary>
	/// Describes text transformation operations that can be executed on a <see cref="StringResource"/>.
	/// </summary>
	public enum TextTransformation
	{
		/// <summary>
		/// Converts all characters in the string to upper case characters.
		/// </summary>
		Upper,
		/// <summary>
		/// Converts all characters in the string to lower case characters.
		/// </summary>
		Lower,
		/// <summary>
		/// Converts the first character of each word in the string to an upper case character.
		/// </summary>
		TitleCase,
		/// <summary>
		/// Converts the first character of the string to an upper case character
		/// </summary>
		FirstWord
	}
}
