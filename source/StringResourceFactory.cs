﻿using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace System.Text.StringResources
{
	/// <summary>
	/// Provider for <see cref="StringResource"/> objects.
	/// </summary>
	public class StringResourceFactory : IStringResource
	{
		#region ... Unmanaged imports ...

		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		private static extern int LoadString(
			IntPtr module,
			int id,
			StringBuilder buffer,
			int bufferSize
		);

		#endregion

		/// <summary>
		/// Represents a regular expression that matches escaped '%' characters.
		/// </summary>
		private static readonly Regex FormatEscapeMatcher = new Regex("(%{2})", RegexOptions.Multiline);

		/// <summary>
		/// Represents the default size that the marshaling buffer for LoadString should have, if no default text is 
		/// provided.
		/// </summary>
		private const int DefaultBufferSize = 1024;

		/// <summary>
		/// Represents the margin with which the length of the default text should be multiplied, to make up the size of
		/// the marshaling buffer for LoadString.
		/// </summary>
		private const double BufferMargin = 1.5;

		/// <summary>
		/// Holds a synchronization object.
		/// </summary>
		private readonly object _syncRoot = new object();

		/// <summary>
		/// Holds a master copy for the resource string.
		/// </summary>
		private StringResource _masterCopy;

		/// <summary>
		/// If the <see cref="StringResourceFactory"/> is instantiated with a value instead of a library and resource
		/// ID, this field will hold the provided value. The value will be used to create a master copy of a
		/// <see cref="StringResource"/> object.
		/// </summary>
		private string _value;

		/// <summary>
		/// Gets the value of the string resource
		/// </summary>
		public string Value
		{
			get { return Get().Value; }
		}

		/// <summary>
		/// Gets the name of library that contains the resource string for this instance.
		/// </summary>
		public string Library { get; private set; }

		/// <summary>
		/// Gets the resource ID for the resource string.
		/// </summary>
		public int Id { get; private set; }

		/// <summary>
		/// Gets the list index of the word or phrase that should be extracted from the string resource. If the string
		/// resource is not a list, the list index should be smaller than 0.
		/// </summary>
		public int ListIndex { get; private set; }

		/// <summary>
		/// Gets the fall-back text that will be used if an error occurs while loading the resource string.
		/// </summary>
		public string Default { get; private set; }

		/// <summary>
		/// Initializes an instance of the <see cref="StringResourceFactory"/>, setting the provided 
		/// <paramref name="value"/> as initialization parameters for the factory operations.
		/// </summary>
		/// <param name="value">The value will be used to create a master copy of a <see cref="StringResource"/> 
		/// object.</param>
		public StringResourceFactory(string value)
		{
			this._value = value;
		}

		/// <summary>
		/// Initializes an instance of the <see cref="StringResourceFactory"/>, setting the provided 
		/// <paramref name="library"/>, <paramref name="id"/> and <paramref name="defaultText"/> as initialization
		/// parameters for the factory operations.
		/// </summary>
		/// <param name="library">Name of the unmanaged library that contains the resource string</param>
		/// <param name="id">ID of the resource string</param>
		/// <param name="defaultText">Fall-back text that will be used if fetching the resource string fails</param>
		public StringResourceFactory(string library, int id, string defaultText)
		{
			Library = library;
			Id = id;
			ListIndex = -1;
			Default = defaultText;
		}

		/// <summary>
		/// Initializes an instance of the <see cref="StringResourceFactory"/>, setting the provided 
		/// <paramref name="library"/>, <paramref name="id"/> and <paramref name="defaultText"/> as initialization
		/// parameters for the factory operations.
		/// </summary>
		/// <param name="library">Name of the unmanaged library that contains the resource string</param>
		/// <param name="id">ID of the resource string</param>
		/// <param name="defaultText">Fall-back text that will be used if the fetching of the resource string fails
		/// </param>
		/// <param name="listIndex">List index of the word or phrase that should be extracted from the resource string
		/// </param>
		public StringResourceFactory(string library, int id, string defaultText, int listIndex)
			: this(library, id, defaultText)
		{
			ListIndex = listIndex;
		}

		/// <summary>
		/// Creates a <see cref="StringResource"/> instance from the configured parameters.
		/// </summary>
		/// <param name="defaultText">Fall-back text that will be used if the fetching of the resource string fails
		/// </param>
		/// <returns><see cref="StringResource"/> instance</returns>
		public StringResource Get(string defaultText = "")
		{
			if (defaultText == "")
			{
				defaultText = Default;
			}

			lock (_syncRoot)
			{
				if (_masterCopy == null)
				{
					_masterCopy = (_value == null)
						? Create(defaultText)
						: new StringResource(_value)
					;
				}
			}

			return _masterCopy.Clone();
		}

		/// <summary>
		/// Creates a <see cref="StringResource"/> instance from the configured parameters.
		/// </summary>
		/// <param name="defaultText">Fall-back text that will be used if the fetching of the resource string fails
		/// </param>
		/// <returns><see cref="StringResource"/> instance</returns>
		private StringResource Create(string defaultText = "")
		{
			try
			{
				// Initialize a StringBuilder that will be marshaled as LPTSTR
				int bufferSize = (defaultText.Length == 0)
					? DefaultBufferSize
					: (int)(defaultText.Length * BufferMargin)
				;

				// Acquire library handle
				IntPtr libraryHandle = UnmanagedLibrary.AcquireHandle(Library);

				// Create buffer and attempt to load the string resource
				StringBuilder loadBuffer = new StringBuilder(bufferSize);
				int size = LoadString(libraryHandle, Id, loadBuffer, bufferSize);

				// Release library handle
				UnmanagedLibrary.ReleaseHandle(Library);

				// Empty string or error returned
				if (size <= 0)
				{
					throw new Exception();
				}

				string buffer = loadBuffer.ToString();

				// Strip formatter escaped character e.g. "%%"
				buffer = FormatEscapeMatcher.Replace(buffer, "%");

				// If no list index was specified (negative list index), return the buffer as it is now
				if (ListIndex < 0)
				{
					return new StringResource(buffer);
				}

				// Ensure the buffer contains a list
				if (!buffer.Contains(";"))
				{
					throw new Exception();
				}

				string[] list = buffer.Split(';');

				// Ensure the list index is within the range of the list
				if (ListIndex >= list.Length)
				{
					throw new IndexOutOfRangeException();
				}

				// Return list item
				return new StringResource(list[ListIndex], this);
			}
			catch (Exception)
			{
				// Return default text
				return new StringResource(defaultText);
			}
		}

		/// <inheritDoc />
		/// <returns>A copy instance of the <see cref="StringResource"/> with the transformation applied</returns>
		public StringResource Transform(TextTransformation transformation)
		{
			return Get().Transform(transformation);
		}

		/// <inheritDoc />
		/// <returns>A copy instance of the <see cref="StringResource"/> with the operation applied</returns>
		public StringResource StripMnemonics()
		{
			return Get().StripMnemonics();
		}

		/// <inheritDoc />
		/// <returns>A copy instance of the <see cref="StringResource"/> with the interpolation applied</returns>
		public StringResource Interpolate(params string[] arguments)
		{
			return Get().Interpolate(arguments);
		}

		/// <summary>
		/// Casts a copy instance of the <see cref="StringResource"/> as a string.
		/// </summary>
		/// <returns>Flattened string representation of the <see cref="StringResource"/></returns>
		public override string ToString()
		{
			return (StringResource)Get();
		}

		/// <summary>
		/// Casts a copy instance of the <see cref="StringResource"/> to a string.
		/// </summary>
		/// <param name="instance">Instance of a <see cref="StringResourceFactory"/></param>
		/// <returns>Flattened string representation of the <see cref="StringResource"/></returns>
		public static implicit operator string(StringResourceFactory instance)
		{
			return (StringResource)instance.Get();
		}
	}
}
