﻿using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace System.Text.StringResources
{
	/// <summary>
	/// Wrapper class around a resource string.
	/// </summary>
	public class StringResource : IStringResource
	{
		/// <summary>
		/// Represents a regular expression that detects mnemonics in a string.
		/// </summary>
		private static readonly Regex MnemonicMatcher = new Regex(
			"(?<!&)(&)",
			RegexOptions.Compiled | RegexOptions.Multiline
		);

		/// <summary>
		/// Represents a regular expression template for the resource interpolation format. 
		/// <a href="https://www.regex101.com/r/kB4rA4/4">Test cases and explanation</a>
		/// </summary>
		private static readonly Regex InterpolationRegex = new Regex(
			@"(%([0-9]+|[dsux])(?:!\.?([0-9]*)(.*?)!)*)",
			RegexOptions.Compiled | RegexOptions.Multiline | RegexOptions.IgnoreCase
		);

		/// <summary>
		/// Gets the value of this <see cref="StringResource"/>
		/// </summary>
		public string Value { get; set; }

		/// <summary>
		/// Gets the factory instance that created this <seealso cref="StringResource"/>, if any.
		/// </summary>
		public StringResourceFactory Factory { get; private set; }

		/// <summary>
		/// Initializes a <see cref="StringResource"/> instance with the provided <paramref name="value"/> as value.
		/// </summary>
		/// <param name="value">Value to initialize the instance with</param>
		public StringResource(string value)
		{
			Value = value;
		}

		/// <summary>
		/// Initializes a <see cref="StringResource"/> instance with the provided <paramref name="value"/> as value, and
		/// the provide <paramref name="factory"/> as source.
		/// </summary>
		/// <param name="value">Value to initialize the instance with</param>
		/// <param name="factory">Factory to set as source</param>
		public StringResource(string value, StringResourceFactory factory)
			: this(value)
		{
			Factory = factory;
		}

		/// <inheritDoc/>
		public StringResource Transform(TextTransformation transformation)
		{
			switch (transformation)
			{
				case TextTransformation.Upper:
					Value = Value.ToUpper();
					break;
				case TextTransformation.Lower:
					Value = Value.ToLower();
					break;
				case TextTransformation.TitleCase:
					Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Value);
					break;
				case TextTransformation.FirstWord:
					Value = Char.ToUpper(Value[0]) + Value.Substring(1);
					break;
			}

			return this;
		}

		/// <inheritDoc/>
		public StringResource StripMnemonics()
		{
			Value = MnemonicMatcher.Replace(Value, "");
			return this;
		}

		/// <inheritDoc/>
		public StringResource Interpolate(params string[] arguments)
		{
			var matches = InterpolationRegex.Matches(Value)
				.Cast<Match>()
				.Where(match => match.Groups.Count == 3 || match.Groups.Count == 5)
				.ToList();

			for (int i = 0; i < matches.Count; i++)
			{
				Match match = matches[i];

				int argumentIndex;
				bool nonIndexedInterpolation = !Int32.TryParse(match.Groups[2].Value, out argumentIndex);
				if (nonIndexedInterpolation)
				{
					argumentIndex = i + 1;
				}
				argumentIndex--;

				if (argumentIndex >= arguments.Length)
				{
					continue;
				}

				string replacementTarget = match.Groups[1].Value;

				if (match.Groups.Count == 3)
				{
					Value = Value.Replace(replacementTarget, arguments[argumentIndex]);
				}

				try
				{
					int padding;
					string format = "{0}";

					if (Int32.TryParse(match.Groups[3].Value, out padding))
					{
						format = "{0}{1}";
					}

					string type = nonIndexedInterpolation ? match.Groups[2].Value : match.Groups[4].Value;

					switch (type)
					{
						case "x":
						case "X":
						case "d":
						case "D":
							Value = Value.Replace(
								replacementTarget,
								Int32.Parse(arguments[argumentIndex]).ToString(
									String.Format(format, type, padding)
								)
							);
							break;
						case "u":
							Value = Value.Replace(
								replacementTarget,
								unchecked((uint)Int32.Parse(arguments[argumentIndex])).ToString(
									String.Format(format, "d", padding)
								)
							);
							break;
						default:
							Value = Value.Replace(replacementTarget, arguments[argumentIndex]);
							break;
					}
				}
				catch
				{
				}
			}
			return this;
		}

		/// <summary>
		/// Returns a clone of this instance.
		/// </summary>
		/// <returns>Cloned <see cref="StringResource"/></returns>
		public StringResource Clone()
		{
			return new StringResource(String.Copy(Value), Factory);
		}

		/// <summary>
		/// Casts a copy instance of the <see cref="StringResource"/> as a string.
		/// </summary>
		/// <returns>Flattened string representation of the <see cref="StringResource"/></returns>
		public override string ToString()
		{
			return this.Value;
		}

		/// <summary>
		/// Casts the provided <paramref name="instance"/> to a string.
		/// </summary>
		/// <param name="instance">Instance of a <see cref="StringResource"/></param>
		/// <returns>Flattened string representation of the <see cref="StringResource"/></returns>
		public static implicit operator string(StringResource instance)
		{
			return (instance == null) ? null : instance.Value;
		}

		/// <summary>
		/// Casts the provided <paramref name="str"/> to a <see cref="StringResource"/> instance.
		/// </summary>
		/// <param name="str">String to cast</param>
		/// <returns><see cref="StringResource"/> instance</returns>
		public static implicit operator StringResource(string str)
		{
			return new StringResource(str);
		}
	}
}
