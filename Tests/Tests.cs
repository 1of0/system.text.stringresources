﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace System.Text.StringResources.Tests
{
	[TestClass]
	public class Tests
	{
		private static readonly StringResourceFactory CaseSimple = new StringResourceFactory("Simple");
		private static readonly StringResourceFactory CaseSimpleSentence = new StringResourceFactory("This is a simple sentence");

		private static readonly StringResourceFactory CaseValidLibraryNoDefault = new StringResourceFactory("shell32.dll", 34128, "");
		private static readonly StringResourceFactory CaseInvalidLibraryDefault = new StringResourceFactory("shouldnotexist.dll", 28745, "Default");
		private static readonly StringResourceFactory CaseInvalidIdDefault = new StringResourceFactory("shell32.dll", -1, "Default");

		private static readonly StringResourceFactory CaseInterpolationFromResource = new StringResourceFactory("shell32.dll", 6481, "%1!ls! (%2!ls!)");
		private static readonly StringResourceFactory CaseRepeatedInterpolation = new StringResourceFactory("%1!ls! %2!ls! %1!ls!");
		private static readonly StringResourceFactory CaseInterpolationUnsigned = new StringResourceFactory("%1!u!");
		private static readonly StringResourceFactory CaseInterpolationHexLower = new StringResourceFactory("%1!x!");
		private static readonly StringResourceFactory CaseInterpolationHexUpper = new StringResourceFactory("%1!X!");
		private static readonly StringResourceFactory CaseInterpolationHexUpperAtLeastTwoCharacters = new StringResourceFactory("%1!2X!");
		private static readonly StringResourceFactory CaseInterpolationNoIndexes = new StringResourceFactory("%d %s %u %x %X");

		private static readonly StringResourceFactory CaseEscapedPercentCharacter = new StringResourceFactory("shell32.dll", 12338, "%d%%");
		private static readonly StringResourceFactory CaseMnemonics = new StringResourceFactory("&mnemonic && &&& &&&&");

		[TestMethod]
		public void TestFactoryValidLibrary()
		{
			CheckEnvironment();

			Assert.AreEqual("%1, %2", CaseValidLibraryNoDefault.Get().ToString());
		}

		[TestMethod]
		public void TestFactoryInvalidLibrary()
		{
			CheckEnvironment();

			Assert.AreEqual("Default", CaseInvalidLibraryDefault.Get().ToString());
		}

		[TestMethod]
		public void TestFactoryInvalidId()
		{
			CheckEnvironment();

			Assert.AreEqual("Default", CaseInvalidIdDefault.Get().ToString());
		}

		[TestMethod]
		public void TestCasting()
		{
			CheckEnvironment();

			// Explicit casts
			Assert.AreEqual("Simple", (string)CaseSimple);
			Assert.AreEqual("Default", (string)CaseInvalidLibraryDefault);

			// Implicit casts
			string nonFactoryResult = CaseSimple;
			string factoryResult = CaseInvalidLibraryDefault;
			Assert.AreEqual("Simple", nonFactoryResult);
			Assert.AreEqual("Default", factoryResult);
		}

		[TestMethod]
		public void TestClone()
		{
			CheckEnvironment();

			StringResource a = CaseSimple.Get();
			StringResource b = a.Clone();

			a.Value = "abc";
			b.Value = "def";

			Assert.AreNotEqual<string>(a, b);
		}

		[TestMethod]
		public void TestInterpolation()
		{
			CheckEnvironment();

			Assert.AreEqual("a (b)", CaseInterpolationFromResource.Interpolate("a", "b").ToString());
			Assert.AreEqual("a b a", CaseRepeatedInterpolation.Interpolate("a", "b").ToString());
			Assert.AreEqual("4294967173", CaseInterpolationUnsigned.Interpolate("-123").ToString());
			Assert.AreEqual("7b", CaseInterpolationHexLower.Interpolate("123").ToString());
			Assert.AreEqual("7B", CaseInterpolationHexUpper.Interpolate("123").ToString());
			Assert.AreEqual("0F", CaseInterpolationHexUpperAtLeastTwoCharacters.Interpolate("15").ToString());
			Assert.AreEqual("123 abc 4294967173 7b 7B", CaseInterpolationNoIndexes.Interpolate("123", "abc", "-123", "123", "123").ToString());

			Assert.AreEqual("a (%2!ls!)", CaseInterpolationFromResource.Interpolate("a").ToString());
			Assert.AreEqual("a %2!ls! a", CaseRepeatedInterpolation.Interpolate("a").ToString());
			Assert.AreEqual("123 abc 4294967173 7b %X", CaseInterpolationNoIndexes.Interpolate("123", "abc", "-123", "123").ToString());
		}

		[TestMethod]
		public void TestTransformation()
		{
			CheckEnvironment();

			Assert.AreEqual<string>("THIS IS A SIMPLE SENTENCE", CaseSimpleSentence.Transform(TextTransformation.Upper));
			Assert.AreEqual<string>("this is a simple sentence", CaseSimpleSentence.Transform(TextTransformation.Lower));
			Assert.AreEqual<string>("This is a simple sentence", CaseSimpleSentence.Transform(TextTransformation.FirstWord));
			Assert.AreEqual<string>("This Is A Simple Sentence", CaseSimpleSentence.Transform(TextTransformation.TitleCase));
		}

		[TestMethod]
		public void TestStripEscapedPercentCharacters()
		{
			CheckEnvironment();

			Assert.AreEqual("5%", CaseEscapedPercentCharacter.Interpolate("5").ToString());
		}

		[TestMethod]
		public void TestStripMnemonics()
		{
			CheckEnvironment();

			Assert.AreEqual("mnemonic & && &&&", CaseMnemonics.StripMnemonics().ToString());
		}

		private void CheckEnvironment()
		{
			if (Environment.OSVersion.Platform != PlatformID.Win32NT)
			{
				Assert.Inconclusive(
					"To test the validity of the fetched string resource, the environment must be a Windows " +
					"installation.\r\n\r\n" +
					"Detected operating system: {0}",
					Environment.OSVersion.Platform
				);
			}
		}
	}
}
